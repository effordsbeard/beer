const path = require('path');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const devMode = process.env.MODE === 'production';

const indexHTML = path.join(__dirname, 'index-src.html');

module.exports = {
  mode: process.env.MODE ? process.env.MODE : 'development',
  entry: [
    './src/main.js',
    './index-src.html'
  ],
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.vue'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'public': path.resolve(__dirname, './public')
    }
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
            'js': 'babel-loader'
          }
        }
      },
      {
        loader: 'iview-loader',
        options: {
            prefix: false
        }
      },
      {
        test: /\.(js)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg|jpeg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              objectAssign: 'Object.assign',
              outputPath: 'assets/img/',
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              // bypassOnDebug: true,
            },
          },
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          objectAssign: 'Object.assign',
          outputPath: 'assets/fonts/'
        }
      },
      {
        test: /\.css$/,
        use: [
          // MiniCssExtractPlugin.loader,
          'css-loader'
        ]
      },
      // {
      //   test: /\.s[ac]ss$/,
      //   use: [
      //     // MiniCssExtractPlugin.loader,
      //     'css-loader?sourceMap',
      //     'sass-loader?indentedSyntax'
      //   ],
      //   resourceQuery: /css/
      // },
      {
        test: /\.sass$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: true,
              sourceMap: true
            }
          }
        ],
        resourceQuery: /load/
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              indentedSyntax: false,
              sourceMap: true
            }
          }
        ],
        resourceQuery: /load/
      },
      {
        test: indexHTML,
        use: [
          {
            loader: "file-loader",
            options: {
                name: "index.html",
                outputPath: '../'
            },
          },
          {
            loader: "extract-loader",
            options: {
              publicPath: 'dist/',
            }
          },
          {
            loader: "html-loader",
            options: {
                attrs: ["img:src"],
                interpolate: true,
            },
          },
        ],
      },
    ]
  },
  devtool: 'source-map',
  plugins: [
    new webpack.LoaderOptionsPlugin({
      context: '/',
      debug: true,
      options: {
        context: __dirname
      }
     }),
    // new MiniCssExtractPlugin({
    //   filename: '[name].css',
    //   // chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    // }),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 3000,
      server: { baseDir: [__dirname] }
    })
  ],
}

if (process.env.MODE === 'production') {
  // module.exports.plugins = (module.exports.plugins || []).concat([])
  module.exports.optimization = {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        uglifyOptions: {
          compress: false,
          ecma: 6,
          mangle: true,
          output: {
            comments: false,
            beautify: false
          }
        },
        sourceMap: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  }
}
